//
//  PickerOptionsViewController.swift
//  ExchangeRates
//
//  Created by Prostoiu Daniel Constantin on 30/01/2020.
//  Copyright © 2020 Prostoiu Daniel Constantin. All rights reserved.
//

import UIKit

class PickerOptionsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    private let tableView = UITableView()
    private let buttonCancel = UIButton()
    private let buttonDone = UIButton()
    
    let cellIdentifier = "CellIdentifier"
    
    var options = [PickerData]()
    var selectedOption: PickerData? = nil
    var onSelectedOptionChanged: ((_ selectedOption: PickerData) -> Void)?
    
    // MARK: view's life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupConstraints()
    }
    
    // MARK: design
    private func setupViews() {
        view.backgroundColor = .init(red: 29 / 255, green: 28 / 255, blue: 30 / 255, alpha: 1)
        
        tableView.register(SizableImageCell.self, forCellReuseIdentifier: cellIdentifier)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = .clear
        tableView.separatorColor = .init(white: 1, alpha: 0.05)
        tableView.dataSource = self
        tableView.delegate = self
        view.addSubview(tableView)
        
        buttonCancel.addTarget(self, action: #selector(btnCancel_onClick), for: .touchUpInside)
        buttonCancel.translatesAutoresizingMaskIntoConstraints = false
        buttonCancel.setTitleColor(.init(red: 243 / 255, green: 163 / 255, blue: 60 / 255, alpha: 1), for: .normal)
        buttonCancel.setTitle("Cancel", for: .normal)
        view.addSubview(buttonCancel)
        
        buttonDone.addTarget(self, action: #selector(btnDone_onClick), for: .touchUpInside)
        buttonDone.translatesAutoresizingMaskIntoConstraints = false
        buttonDone.setTitleColor(.init(red: 243 / 255, green: 163 / 255, blue: 60 / 255, alpha: 1), for: .normal)
        buttonDone.setTitle("Done", for: .normal)
        view.addSubview(buttonDone)
    }
    
    private func setupConstraints() {
        let constraints = [
            buttonCancel.topAnchor.constraint(equalTo: view.topAnchor, constant: 4),
            buttonCancel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 8),
            buttonCancel.widthAnchor.constraint(equalToConstant: 80),
            buttonCancel.heightAnchor.constraint(equalToConstant: 40),

            buttonDone.topAnchor.constraint(equalTo: view.topAnchor, constant: 4),
            buttonDone.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -8),
            buttonDone.widthAnchor.constraint(equalToConstant: 66),
            buttonDone.heightAnchor.constraint(equalToConstant: 40),
            
            tableView.topAnchor.constraint(equalTo: view.topAnchor, constant: 48),
            tableView.leftAnchor.constraint(equalTo: view.leftAnchor),
            tableView.rightAnchor.constraint(equalTo: view.rightAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
    
    // MARK: button actions
    @objc private func btnCancel_onClick() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc private func btnDone_onClick() {
        if selectedOption != nil {
            onSelectedOptionChanged?(selectedOption!)
        }
    }
}

fileprivate class SizableImageCell: UITableViewCell {
    override func layoutSubviews() {
        super.layoutSubviews()
        imageView?.frame = .init(x: 20, y: bounds.midY - 8, width: 16, height: 16)
        textLabel?.frame = CGRect(x: 49, y: textLabel!.frame.origin.y, width: textLabel!.frame.size.width, height: textLabel!.frame.size.height)
        separatorInset = .init(top: 0, left: 50, bottom: 0, right: 0)
    }
}
