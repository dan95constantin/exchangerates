//
//  Picker.swift
//  ExchangeRates
//
//  Created by Daniel Constantin Prostoiu on 30/01/2020.
//  Copyright © 2020 Prostoiu Daniel Constantin. All rights reserved.
//

import UIKit

class PickerView: UIButton {
    weak var delegate: UIViewController? = nil
    var options = [PickerData]()
    var selectedOption: PickerData? {
        didSet {
            lblSelected.text = selectedOption != nil ? selectedOption?.value : ""
        }
    }
    
    var title: String? {
        didSet {
            lblTitle.text = title
        }
    }
    
    private let lblTitle = UILabel()
    private let lblSelected = UILabel()
    private let imageRightArrow = UIImageView()
    
    // MARK: view's life cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToSuperview() {
        if superview != nil {
            setupConstraints()
        }
    }
    
    // MARK: design
    private func setupViews() {
        addTarget(self, action: #selector(btnShowPicker_onClick), for: .touchUpInside)
        backgroundColor = .init(red: 26 / 255, green: 26 / 255, blue: 28 / 255, alpha: 1)
        translatesAutoresizingMaskIntoConstraints = false
        layer.cornerRadius = 12
        
        lblTitle.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)
        lblTitle.setContentHuggingPriority(.defaultLow, for: .horizontal)
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        lblTitle.font = .systemFont(ofSize: 14)
        lblTitle.textColor = .lightGray
        addSubview(lblTitle)
        
        lblSelected.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)
        lblSelected.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        lblSelected.translatesAutoresizingMaskIntoConstraints = false
        lblSelected.font = .systemFont(ofSize: 14)
        lblSelected.textColor = .lightGray
        addSubview(lblSelected)
        
        imageRightArrow.translatesAutoresizingMaskIntoConstraints = false
        imageRightArrow.image = UIImage.init(named: "icon_arrow_right")?.tinted(color: .lightGray)
        addSubview(imageRightArrow)
    }
    
    private func setupConstraints() {
        let constraints = [
            heightAnchor.constraint(equalToConstant: 50),
            
            lblTitle.leftAnchor.constraint(equalTo: leftAnchor, constant: 12),
            lblTitle.rightAnchor.constraint(equalTo: lblSelected.leftAnchor, constant: -8),
            lblTitle.centerYAnchor.constraint(equalTo: centerYAnchor),
            
            lblSelected.rightAnchor.constraint(equalTo: imageRightArrow.leftAnchor, constant: -8),
            lblSelected.centerYAnchor.constraint(equalTo: centerYAnchor),
            
            imageRightArrow.rightAnchor.constraint(equalTo: rightAnchor, constant: -12),
            imageRightArrow.centerYAnchor.constraint(equalTo: centerYAnchor),
            imageRightArrow.widthAnchor.constraint(equalToConstant: 14),
            imageRightArrow.heightAnchor.constraint(equalToConstant: 14)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
    
    // MARK: button actions
    @objc private func btnShowPicker_onClick() {
        let viewController = PickerOptionsViewController()
        viewController.options = options
        viewController.selectedOption = selectedOption
        viewController.onSelectedOptionChanged = { [weak self] (selectedOption: PickerData) in
            self?.selectedOption = selectedOption
            self?.delegate?.dismiss(animated: true, completion: nil)
        }
        
        delegate?.present(viewController, animated: true, completion: nil)
    }
}
