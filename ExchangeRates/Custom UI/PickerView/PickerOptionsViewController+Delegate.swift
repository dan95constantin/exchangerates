//
//  PickerOptionsViewController+Delegate.swift
//  ExchangeRates
//
//  Created by Prostoiu Daniel Constantin on 30/01/2020.
//  Copyright © 2020 Prostoiu Daniel Constantin. All rights reserved.
//

import UIKit

extension PickerOptionsViewController {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedOption = options[indexPath.row]
        tableView.reloadData()
    }
}
