//
//  PickerData.swift
//  ExchangeRates
//
//  Created by Prostoiu Daniel Constantin on 29/01/2020.
//  Copyright © 2020 Prostoiu Daniel Constantin. All rights reserved.
//

struct PickerData {
    let key: String
    let value: String
}
