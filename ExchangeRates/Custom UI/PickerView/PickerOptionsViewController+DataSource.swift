//
//  PickerOptionsViewController+DataSource.swift
//  ExchangeRates
//
//  Created by Prostoiu Daniel Constantin on 30/01/2020.
//  Copyright © 2020 Prostoiu Daniel Constantin. All rights reserved.
//

import UIKit

extension PickerOptionsViewController {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        let option = options[indexPath.row]
        
        if selectedOption != nil && option.key == selectedOption!.key {
            cell.imageView?.image = UIImage.init(named: "icon_checkmark")?.tinted(color: .init(red: 243 / 255, green: 163 / 255, blue: 60 / 255, alpha: 1))
        } else {
            cell.imageView?.image = nil
        }
        
        cell.textLabel?.text = option.value
        cell.textLabel?.textColor = .init(white: 1, alpha: 0.8)
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        return cell
    }
}
