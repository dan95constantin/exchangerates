//
//  LineChartView.swift
//  ExchangeRates
//
//  Created by Prostoiu Daniel Constantin on 28/01/2020.
//  Copyright © 2020 Prostoiu Daniel Constantin. All rights reserved.
//

import UIKit
import Foundation

let offsetY: CGFloat = 6.5
let offsetX: CGFloat = 50
let xValueDefaultWidth: CGFloat = 50
let defaultFont = UIFont.systemFont(ofSize: 10.0)

class LineChartView: UIScrollView {
    var values: [DateValue] = [] {
        didSet {
            setupContentView()
        }
    }
    
    // MARK: view's life cycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
        showsVerticalScrollIndicator = false
        showsHorizontalScrollIndicator = false
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupContentView() {
        let chartView = LineChart()
        let chartWidth = offsetX + xValueDefaultWidth * CGFloat(values.count)
        chartView.values = values
        chartView.frame = .init(x: 0, y: 0, width: Int(chartWidth), height: values.count * 30 + 10)
        addSubview(chartView)
        
        frame = .init(x: Int(frame.origin.x), y: Int(frame.origin.y), width: Int(frame.width), height: values.count * 30 + 10)
        contentSize = .init(width: chartWidth, height: CGFloat(values.count * 30 + 10))
    }
    
//    override func didMoveToSuperview() {
//        if superview != nil {
//            translatesAutoresizingMaskIntoConstraints = false
//            
//            let constraints = [
//                topAnchor.constraint(equalTo: superview!.topAnchor),
//                leftAnchor.constraint(equalTo: superview!.leftAnchor),
//                rightAnchor.constraint(equalTo: superview!.rightAnchor),
//                bottomAnchor.constraint(equalTo: superview!.bottomAnchor),
//            ]
//            
//            NSLayoutConstraint.activate(constraints)
//        }
//    }
}

fileprivate class LineChart: UIView {
    var width: CGFloat!
    var height: CGFloat!
    var minValue: DateValue?
    var maxValue: DateValue?
    var yValues = [Double]()
    
    var values: [DateValue] = [] {
        didSet {
            minValue = values.max{ $0.value > $1.value }
            maxValue = values.min{ $0.value > $1.value }
            yValues = values.map({ value in
                return value.value
            })
            yValues = yValues.sorted(by: { $0 > $1 })
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        clipsToBounds = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToSuperview() {
        width = frame.width
        height = frame.height - 20
    }
    
    override func draw(_ rect: CGRect) {
        if values.count > 0 {
            let linePath = UIBezierPath()
            linePath.lineWidth = 0.75
            UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.1).set()
            
            let chartPath = UIBezierPath()
            chartPath.lineWidth = 2
            UIColor.lightGray.set()
            
            let attributes: [NSAttributedString.Key : Any] = [
                .paragraphStyle: NSMutableParagraphStyle(),
                .foregroundColor: UIColor.lightGray,
                .font: defaultFont
            ]
            var attributedString: NSAttributedString
            
            for (index, element) in values.enumerated() {
                let xValueWidth = element.graphDate.widthOfString(usingFont: defaultFont)
                linePath.move(to: CGPoint(x: offsetX, y: CGFloat(index * 30) + offsetY))
                linePath.addLine(to: CGPoint(x: width, y: CGFloat(index * 30) + offsetY))
                
                attributedString = NSAttributedString(string: "\(yValues[index])", attributes: attributes)
                attributedString.draw(in: .init(x: 0, y: CGFloat(index * 30), width: 50, height: 20))
                
                attributedString = NSAttributedString(string: element.graphDate, attributes: attributes)
                attributedString.draw(in: .init(x: CGFloat(index * Int(xValueDefaultWidth)) + offsetX, y: height, width: xValueDefaultWidth, height: 20))
                
                if (index == 0) {
                    chartPath.move(to: CGPoint(x: offsetX + xValueWidth / 2.0, y: CGFloat((values.count - 1) * 30) + offsetY))
                }
                
                let yValueIndex = yValues.firstIndex(of: element.value)!
                chartPath.addLine(to: CGPoint(x: offsetX + xValueWidth / 2.0 + CGFloat(CGFloat(index) * xValueDefaultWidth), y: CGFloat(yValueIndex * 30) + offsetY))
                
                if (index == values.count - 1) {
                    chartPath.addLine(to: CGPoint(x: offsetX + xValueWidth / 2.0 + CGFloat(CGFloat(index) * xValueDefaultWidth), y: CGFloat((values.count - 1) * 30) + offsetY))
                }
            }
            
            linePath.stroke()
            chartPath.stroke()
            
            let frame = chartPath.bounds
            let gradient = CAGradientLayer()
            gradient.frame = CGRect(x: 0, y: 0, width: frame.width * 2, height: frame.height * 2 - 50)
            gradient.colors = [UIColor.gray.cgColor, UIColor.clear.cgColor]
            
            let shapeMask = CAShapeLayer()
            shapeMask.path = chartPath.cgPath
            gradient.mask = shapeMask
            
            layer.addSublayer(gradient)
        }
    }
}
