//
//  BaseViewController.swift
//  ExchangeRates
//
//  Created by Prostoiu Daniel Constantin on 28/01/2020.
//  Copyright © 2020 Prostoiu Daniel Constantin. All rights reserved.
//

import UIKit

// Base view controller subclasses by every screen to change the background from one place
class BaseViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .black
    }
}
