//
//  Constants.swift
//  ExchangeRates
//
//  Created by Prostoiu Daniel Constantin on 28/01/2020.
//  Copyright © 2020 Prostoiu Daniel Constantin. All rights reserved.
//

// Keys for UserDefaults for saving and retriving selected currency and refresh interval that are set in settings screen
enum UserDefaultKeys: String {
    case defaultCurrency = "defaultCurrency"
    case defaultRefreshRate = "defaultRefreshRate"
}

// Configuration for graphic screen to load past 10 days values for configured currencies and for settings screen to select different refresh interval for currencies list
struct Config {
    static let graphicCurrencies = ["RON", "USD", "BGN"]
    static let refreshIntervals = [3, 5, 15]
    static let defaultCurrency = "EUR"
    static let defaultRefreshRate = 3
}

// Generate api urls for requests
struct API {
    static let root = "https://api.exchangeratesapi.io"
    
    static func graphic(start s: String, end e: String, base b: String, symbols sy: String) -> String {
        return "\(API.root)/history?start_at=\(s)&end_at=\(e)&base=\(b)&symbols=\(sy)"
    }
    
    static func currencies(currencyIso cIso: String) -> String {
        return "\(API.root)/latest?base=\(cIso)"
    }
    
    static func settings() -> String {
        return "\(API.root)/latest"
    }
}
