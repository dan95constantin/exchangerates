//
//  CurrenciesViewModel.swift
//  ExchangeRates
//
//  Created by Prostoiu Daniel Constantin on 28/01/2020.
//  Copyright © 2020 Prostoiu Daniel Constantin. All rights reserved.
//

import Foundation
import Alamofire

typealias CurrenciesCompletionHandler = (() -> Void)

class CurrenciesViewModel {
    // properties
    var refreshDate = ""
    private var currencies = [Currency]()
    private var observers = [String: CurrenciesCompletionHandler]()
    private let dateFormatter = DateFormatter()
    
    init() {
        dateFormatter.dateFormat = "HH:mm:ss"
    }
    
    // adding callback observers to notify the view controller
    func addObserver(observer: NSObject, completionHandler: @escaping CurrenciesCompletionHandler) {
        observers[observer.description] = completionHandler
    }
    
    private func notify() {
        observers.forEach({ $0.value() })
    }
    
    deinit {
        observers.removeAll()
    }
    
    // fetch currencies bases on required currency iso and parse for currencies list screen
    func fetchCurrencies(currencyIso: String) {
        AF.request(API.currencies(currencyIso: currencyIso)).responseJSON { response in
            if self.parseCurrencies(responseJson: response.value, currencyIso: currencyIso) {
                print("a mers")
            } else {
                print("nu a mers")
            }
            
            self.saveRefreshDate()
            self.notify()
        }
    }
    
    private func parseCurrencies(responseJson: Any?, currencyIso: String) -> Bool {
        guard let json = responseJson as? [String: Any] else {
            return false
        }
        
        guard let rates = json["rates"] as? [String: Any] else {
            return false
        }
        
        if rates.count > 0 {
            currencies.removeAll()
            
            rates.keys.sorted().forEach { key in
                let decimalRate = rates[key] as? Double
                currencies.append(Currency(currencyIso: key, baseIso: currencyIso, rate: decimalRate!))
            }
        }
        
        return true
    }
    
    // save refrash date
    private func saveRefreshDate() {
        refreshDate = "Refreshed at \(dateFormatter.string(from: Date()))"
    }
}

// access data from private currencies list
extension CurrenciesViewModel {
    func currenciesCount() -> Int {
        return currencies.count
    }
    
    func currencyVM(at index: Int) -> CurrencyVM {
        return CurrencyVM(currency: currencies[index])
    }
}

// map currency data on what we need to not use currency diretly in view controller
struct CurrencyVM {
    private let currency: Currency
    
    var exchange: String {
        return "1 \(currency.baseIso) = \(currency.rate) \(currency.currencyIso)"
    }
    
    init(currency: Currency) {
        self.currency = currency
    }
}
