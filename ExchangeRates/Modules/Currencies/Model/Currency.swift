//
//  Currency.swift
//  ExchangeRates
//
//  Created by Prostoiu Daniel Constantin on 28/01/2020.
//  Copyright © 2020 Prostoiu Daniel Constantin. All rights reserved.
//

struct Currency {
    let currencyIso: String
    let baseIso: String
    let rate: Double
}
