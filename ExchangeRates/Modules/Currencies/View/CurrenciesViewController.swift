//
//  CurrenciesViewController.swift
//  ExchangeRates
//
//  Created by Prostoiu Daniel Constantin on 28/01/2020.
//  Copyright © 2020 Prostoiu Daniel Constantin. All rights reserved.
//

import UIKit

class CurrenciesViewController: BaseViewController, UITableViewDataSource {
    let lblRefreshDate = UILabel()
    let tableView = UITableView()
    let currenciesViewModal = CurrenciesViewModel()
    var currencyRefreshTimer: Timer!
    
    let cellIdentifier = "CellIdentifier"
    // default configuration if nothing is saved in UserDefaults
    var defaultCurrency = "EUR"
    var defaultRefreshRate: TimeInterval = 3
    
    // MARK: view's life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupConstraints()
        customizeNavigationBar()
        
        // add observer to view model to be notified after data is downloaded
        currenciesViewModal.addObserver(observer: self) { [weak self] in
            self?.tableView.reloadData()
            self?.lblRefreshDate.text = self?.currenciesViewModal.refreshDate
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // get saved currency and refresh interval from settings in case it has been changed
        getSavedCurrencyAndRefreshInterval()
        // fetch currencies when screen is shown
        fetchCurrencies()
        // setup Timer to fetch currencies again after selected refresh rate
        setupRefreshInterval()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        // invalidate Timer when leaving the screen
        currencyRefreshTimer.invalidate()
    }
    
    // MARK: business logic
    private func setupRefreshInterval() {
        currencyRefreshTimer = Timer.scheduledTimer(timeInterval: defaultRefreshRate, target: self, selector: #selector(fetchCurrencies), userInfo: nil, repeats: true)
    }
    
    @objc private func fetchCurrencies() {
        currenciesViewModal.fetchCurrencies(currencyIso: defaultCurrency)
    }
    
    private func getSavedCurrencyAndRefreshInterval() {
        if let defaultCurrency = UserDefaults.standard.string(forKey: UserDefaultKeys.defaultCurrency.rawValue) {
            self.defaultCurrency = defaultCurrency
        }
        
        if let defaultRefreshRate = UserDefaults.standard.string(forKey: UserDefaultKeys.defaultRefreshRate.rawValue) {
            self.defaultRefreshRate = Double(defaultRefreshRate)!
        }
    }
    
    // MARK: design
    private func customizeNavigationBar() {
        title = "Currencies"
        
        let settings = UIBarButtonItem(image: UIImage.init(named: "icon_settings"), style: .plain, target: self, action: #selector(btnOpenSettings_onClick))
        let historic = UIBarButtonItem(image: UIImage.init(named: "icon_historic"), style: .plain, target: self, action: #selector(btnOpenHistoric_onClick))
        navigationItem.rightBarButtonItems = [settings, historic]
        navigationController?.navigationBar.tintColor = .init(red: 29 / 255, green: 28 / 255, blue: 30 / 255, alpha: 1)
    }
    
    private func setupViews() {
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellIdentifier)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = .clear
        tableView.dataSource = self
        view.addSubview(tableView)
        
        lblRefreshDate.translatesAutoresizingMaskIntoConstraints = false
        lblRefreshDate.textColor = .lightGray
        view.addSubview(lblRefreshDate)
    }
    
    private func setupConstraints() {
        let constraints = [
            lblRefreshDate.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 8),
            lblRefreshDate.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20),
            lblRefreshDate.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 16),
            lblRefreshDate.bottomAnchor.constraint(equalTo: tableView.topAnchor, constant: -8),
            
            tableView.leftAnchor.constraint(equalTo: view.leftAnchor),
            tableView.rightAnchor.constraint(equalTo: view.rightAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
    
    // MARK: button actions
    @objc private func btnOpenHistoric_onClick() {
        navigationController?.pushViewController(GraphicViewController(), animated: true)
    }
    
    @objc private func btnOpenSettings_onClick() {
        navigationController?.pushViewController(SettingsViewController(), animated: true)
    }
}
