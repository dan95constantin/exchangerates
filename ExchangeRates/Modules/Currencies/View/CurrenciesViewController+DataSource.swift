//
//  CurrenciesViewController+DataSource.swift
//  ExchangeRates
//
//  Created by Prostoiu Daniel Constantin on 29/01/2020.
//  Copyright © 2020 Prostoiu Daniel Constantin. All rights reserved.
//

import UIKit

extension CurrenciesViewController {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currenciesViewModal.currenciesCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        let currencyVM = currenciesViewModal.currencyVM(at: indexPath.row)
        cell.textLabel?.text = currencyVM.exchange
        cell.textLabel?.textColor = .lightGray
        cell.backgroundColor = .clear
        cell.selectionStyle = .none
        return cell
    }
}
