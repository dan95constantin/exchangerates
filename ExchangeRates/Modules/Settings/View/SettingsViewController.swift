//
//  SettingsViewController.swift
//  ExchangeRates
//
//  Created by Prostoiu Daniel Constantin on 28/01/2020.
//  Copyright © 2020 Prostoiu Daniel Constantin. All rights reserved.
//

import UIKit

class SettingsViewController: BaseViewController {
    let currencyPickerView = PickerView()
    let refreshPickerView = PickerView()
    
    let settingsViewModel = SettingsViewModel()
    
    // MARK: view's life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupConstraints()
        customizeNavigationBar()
        setDefaultOptionsToPickers()
        setRefreshPickerOptions()
        
        // fetch currencies
        settingsViewModel.fetchCurrencyISOs()
        // add observer to be notified after currencies are downloaded
        settingsViewModel.addObserver(observer: self) { [weak self] in
            // map currency ISOs to picker's custom data format. In this way the picker can be reusable
            self?.currencyPickerView.options = (self?.settingsViewModel.currencyISOs)!
        }
    }
    
    // save pickers selected values when screen is closed
    override func viewWillDisappear(_ animated: Bool) {
        UserDefaults.standard.set(currencyPickerView.selectedOption?.key, forKey: UserDefaultKeys.defaultCurrency.rawValue)
        UserDefaults.standard.set(refreshPickerView.selectedOption?.key, forKey: UserDefaultKeys.defaultRefreshRate.rawValue)
    }
    
    // MARK: design
    private func customizeNavigationBar() {
        title = "Settings"
    }
    
    private func setupViews() {
        currencyPickerView.delegate = self
        currencyPickerView.title = "Selected currency"
        view.addSubview(currencyPickerView)
        
        refreshPickerView.delegate = self
        refreshPickerView.title = "Selected refresh interval"
        view.addSubview(refreshPickerView)
    }
    
    private func setupConstraints() {
        let constraints = [
            currencyPickerView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20),
            currencyPickerView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16),
            currencyPickerView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16),
            
            refreshPickerView.topAnchor.constraint(equalTo: currencyPickerView.bottomAnchor, constant: 8),
            refreshPickerView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16),
            refreshPickerView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
    
    // MARK: business logic
    private func setDefaultOptionsToPickers() {
        if let defaultCurrency = UserDefaults.standard.string(forKey: UserDefaultKeys.defaultCurrency.rawValue) {
            currencyPickerView.selectedOption = PickerData(key: defaultCurrency, value: defaultCurrency)
        }
        
        if let defaultRefreshRate = UserDefaults.standard.string(forKey: UserDefaultKeys.defaultRefreshRate.rawValue) {
            refreshPickerView.selectedOption = PickerData(key: defaultRefreshRate, value: defaultRefreshRate)
        }
    }
    
    // set data for refreh interval picker based on Config
    private func setRefreshPickerOptions() {
        refreshPickerView.options = Config.refreshIntervals.map({
            PickerData(key: "\($0)", value: "\($0)")
        })
    }
}
