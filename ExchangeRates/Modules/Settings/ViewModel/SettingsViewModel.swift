//
//  SettingsViewModel.swift
//  ExchangeRates
//
//  Created by Daniel Constantin Prostoiu on 30/01/2020.
//  Copyright © 2020 Prostoiu Daniel Constantin. All rights reserved.
//

import Alamofire

typealias SettingsCompletionHandler = (() -> Void)

class SettingsViewModel {
    // private properties
    var currencyISOs = [PickerData]()
    private var observers = [String: SettingsCompletionHandler]()
    
    // adding observer to notify the view controller
    func addObserver(observer: NSObject, completionHandler: @escaping SettingsCompletionHandler) {
        observers[observer.description] = completionHandler
    }
    
    private func notify() {
        observers.forEach({ $0.value() })
    }
    
    deinit {
        observers.removeAll()
    }
    
    // fetch all currencies
    func fetchCurrencyISOs() {
        AF.request(API.settings()).responseJSON { response in
            if self.parseCurrencyISOs(responseJson: response.value) {
                print("a mers")
            } else {
                print("nu a mers")
            }
            
            self.notify()
        }
    }
    
    // parse currencies to get the ISOs and insert EUR because is not in the api list
    private func parseCurrencyISOs(responseJson: Any?) -> Bool {
        guard let json = responseJson as? [String: Any] else {
            return false
        }
        
        guard let rates = json["rates"] as? [String: Any] else {
            return false
        }
        
        if rates.count > 0 {
            currencyISOs.append(PickerData(key: "EUR", value: "EUR"))
            
            rates.keys.forEach { key in
                currencyISOs.append(PickerData(key: key, value: key))
            }
            
            currencyISOs = currencyISOs.sorted(by: { $0.key < $1.key })
        }
        
        return true
    }
}
