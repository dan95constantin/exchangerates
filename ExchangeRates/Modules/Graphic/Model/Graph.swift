//
//  Graph.swift
//  ExchangeRates
//
//  Created by Prostoiu Daniel Constantin on 29/01/2020.
//  Copyright © 2020 Prostoiu Daniel Constantin. All rights reserved.
//

import Foundation

struct Graph {
    let currencyIso: String
    let values: Array<DateValue>
}
