//
//  GraphicViewModel.swift
//  ExchangeRates
//
//  Created by Prostoiu Daniel Constantin on 29/01/2020.
//  Copyright © 2020 Prostoiu Daniel Constantin. All rights reserved.
//

import Foundation
import Alamofire

typealias GraphicCompletionHandler = (() -> Void)

class GraphicViewModel {
    // private properties
    private var graphics = [Graph]()
    private var observers = [String: GraphicCompletionHandler]()
    
    // adding observer to notify the view controller
    func addObserver(observer: NSObject, completionHandler: @escaping GraphicCompletionHandler) {
        observers[observer.description] = completionHandler
    }
    
    private func notify() {
        observers.forEach({ $0.value() })
    }
    
    deinit {
        observers.removeAll()
    }
    
    // fetch currencies from last 10 days based on EUR
    func fetchCurrencies() {
        let url = API.graphic(start: Date().toGraphFormat10DaysAgo, end: Date().toGraphFormatNow, base: "EUR", symbols: Config.graphicCurrencies.joined(separator:","))
        
        AF.request(url).responseJSON { response in
            if self.parseCurrencies(responseJson: response.value) {
                print("a mers")
            } else {
                print("nu a mers")
            }
            
            self.notify()
        }
    }
    
    // parse currencies
    private func parseCurrencies(responseJson: Any?) -> Bool {
        guard let json = responseJson as? [String: Any] else {
            return false
        }
        
        guard let rates = json["rates"] as? [String: Any] else {
            return false
        }
        
        if rates.count > 0 {
            let dateKeys = rates.keys.sorted()
            let requestDateFormatter = DateFormatter()
            let graphDateFormatter = DateFormatter()
            requestDateFormatter.dateFormat = "yyyy-MM-dd"
            graphDateFormatter.dateFormat = "MMM dd"
            
            Config.graphicCurrencies.forEach { currencyIso in
                var values = [DateValue]()
                
                dateKeys.forEach { dateKey in
                    guard let rate = rates[dateKey] as? [String: Any] else {
                        return
                    }
                    
                    let decimalRate = rate[currencyIso] as? Double
                    let date = requestDateFormatter.date(from: dateKey)
                    values.append(DateValue(date: dateKey, value: decimalRate!, graphDate: graphDateFormatter.string(from: date!)))
                }
                
                graphics.append(Graph(currencyIso: currencyIso, values: values))
            }
        }
        
        return true
    }
}

// access data from private graphics list
extension GraphicViewModel {
    var graphicCount: Int {
        return graphics.count
    }
    
    func graphVM(at index: Int) -> GraphVM {
        return GraphVM(graph: graphics[index])
    }
}

// map graph data on what we need to not use graphics diretly in view controller
struct GraphVM {
    private let graph: Graph
    
    init(graph: Graph) {
        self.graph = graph
    }
    
    var currencyIso: String {
        return self.graph.currencyIso
    }
    
    var values: [DateValue] {
        return self.graph.values
    }
}
