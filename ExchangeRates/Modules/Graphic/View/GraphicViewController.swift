//
//  GraphicViewController.swift
//  ExchangeRates
//
//  Created by Prostoiu Daniel Constantin on 28/01/2020.
//  Copyright © 2020 Prostoiu Daniel Constantin. All rights reserved.
//

import UIKit

class GraphicViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {
    private let tableView = UITableView()
    let graphicViewModel = GraphicViewModel()
    let cellIdentifier = "CellIdentifier"
    
    // MARK: view's life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setupConstraints()
        customizeNavigationBar()
        
        // fetch currencies
        graphicViewModel.fetchCurrencies()
        // add observer to be notified after currencies are downloaded
        graphicViewModel.addObserver(observer: self) { [weak self] in
            self?.tableView.reloadData()
        }
    }
    
    // MARK: design
    private func customizeNavigationBar() {
        title = "Historic"
    }
    
    private func setupViews() {
        tableView.register(GraphicCell.self, forCellReuseIdentifier: cellIdentifier)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.separatorColor = .clear
        tableView.dataSource = self
        tableView.backgroundColor = .clear
        view.addSubview(tableView)
    }
    
    private func setupConstraints() {
        let constraints = [
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 8),
            tableView.leftAnchor.constraint(equalTo: view.leftAnchor),
            tableView.rightAnchor.constraint(equalTo: view.rightAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
}
