//
//  GraphicCell.swift
//  ExchangeRates
//
//  Created by Prostoiu Daniel Constantin on 29/01/2020.
//  Copyright © 2020 Prostoiu Daniel Constantin. All rights reserved.
//

import UIKit

class GraphicCell: UITableViewCell {
    let lblCurrency = UILabel()
    let chart = LineChartView()
    let viewChartContainer = UIView()
    
    // MARK: cell's life cycle
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: design
    private func setupViews() {
        backgroundColor = .clear
        
        lblCurrency.translatesAutoresizingMaskIntoConstraints = false
        lblCurrency.textColor = .lightGray
        addSubview(lblCurrency)
        
        viewChartContainer.translatesAutoresizingMaskIntoConstraints = false
        viewChartContainer.backgroundColor = .clear
        addSubview(viewChartContainer)
        
        let screenWidth = UIScreen.main.bounds.width - 32
        chart.frame = .init(x: 16, y: 0, width: screenWidth, height: 190)
        viewChartContainer.addSubview(chart)
    }
    
    private func setupConstraints() {
        let constraints = [
            lblCurrency.topAnchor.constraint(equalTo: topAnchor, constant: 8),
            lblCurrency.leftAnchor.constraint(equalTo: leftAnchor, constant: 16),
            lblCurrency.rightAnchor.constraint(equalTo: rightAnchor, constant: 8),
            lblCurrency.bottomAnchor.constraint(equalTo: viewChartContainer.topAnchor, constant: -8),
            
            viewChartContainer.leftAnchor.constraint(equalTo: leftAnchor),
            viewChartContainer.rightAnchor.constraint(equalTo: rightAnchor),
            viewChartContainer.bottomAnchor.constraint(equalTo: bottomAnchor),
            viewChartContainer.heightAnchor.constraint(equalToConstant: 190)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
}
