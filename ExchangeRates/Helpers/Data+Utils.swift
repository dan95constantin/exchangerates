//
//  Data+Utils.swift
//  ExchangeRates
//
//  Created by Prostoiu Daniel Constantin on 29/01/2020.
//  Copyright © 2020 Prostoiu Daniel Constantin. All rights reserved.
//

import UIKit

extension Date {
    // Return now date in yyyy-MM-dd format graphic request
    var toGraphFormatNow: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: self)
    }
    
    // Return 10 days ago date in yyyy-MM-dd format for graphic request
    var toGraphFormat10DaysAgo: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: Calendar.current.date(byAdding: .day, value: -10, to: self)!)
    }
}
