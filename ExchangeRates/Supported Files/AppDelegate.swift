//
//  AppDelegate.swift
//  ExchangeRates
//
//  Created by Prostoiu Daniel Constantin on 28/01/2020.
//  Copyright © 2020 Prostoiu Daniel Constantin. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        saveInitialConfiguration()
        
        window = .init(frame: UIScreen.main.bounds)
        window?.rootViewController = UINavigationController(rootViewController: CurrenciesViewController())
        window?.makeKeyAndVisible()
        return true
    }
    
    private func saveInitialConfiguration() {
        if UserDefaults.standard.string(forKey: UserDefaultKeys.defaultCurrency.rawValue) == nil {
            UserDefaults.standard.set(Config.defaultCurrency, forKey: UserDefaultKeys.defaultCurrency.rawValue)
        }
        
        if UserDefaults.standard.string(forKey: UserDefaultKeys.defaultRefreshRate.rawValue) == nil {
            UserDefaults.standard.set(Config.defaultRefreshRate, forKey: UserDefaultKeys.defaultRefreshRate.rawValue)
        }
    }
}
